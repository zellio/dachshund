

(ns dachshund.Archive
  (:import (java.util.zip ZipFile)
           (dachshund Manifest))
  (:gen-class
   :extends java.lang.Object
   :init init
   :constructors {[java.lang.String] []}
   :main false
   :state state
   :prefix "bb:archive-"
   :methods [[open [] java.util.zip.ZipFile]
             [close [] void]
             [getFile [java.lang.String clojure.lang.IFn] java.lang.Object]
             [manifest [] edu.nyu.blackboard.util.dachshund.Manifest]
             [containsAssessment [] java.lang.Boolean]]))


(defn open? [ this ]
  (:fh @(.state this)))

(defn bb:archive-init [ path ]
  [[] (atom {:p path :fh nil :m nil})])

(defn bb:archive-open [ this ]
  (let [s (.state this)]
    (or (:fh @s) (:fh (swap! s assoc-in [:fh] (ZipFile. (:p @s)))))))

(defn bb:archive-close [ this ]
  (let [s (.state this)
        fh (:fh @s)]
    (when fh
      (.close fh))
    (swap! s assoc-in [:fh] nil)))

(defn bb:archive-getFile [ this file transform ]
  (try
    (let [fh (.open this)]
      (if-let [e (.getEntry fh file)]
        (let [t (transform (.getInputStream fh e))]
          (.close this) t)))
    (catch Exception e
      (binding [*out* *err*]
        (println "FAILED TO GET FILE <" file "> IN ARCHIVE <" this ">")))))

(defn bb:archive-manifest [ this ]
  (let [s (.state this)]
    (or
     (:m @s)
     (:m (swap! s assoc-in [:m] (Manifest. this))))))

(defn bb:archive-toString [ this ]
  (:p @(.state this)))

(defn bb:archive-containsAssessment [ this ]
  (.hasAssessment (.manifest this)))
